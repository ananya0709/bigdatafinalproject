package com.service.writeprototohdfs;

import com.config.HadoopConfiguration;
import com.config.VariableSetter;
import com.util.ConfigurationClass;
import com.util.IProtoHDFSWriter;
import com.util.SequenceFileWriter;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.util.ArrayList;

public class EmployeeProtoHDFSWriter implements IProtoHDFSWriter {
    ArrayList<EmployeeOuterClass.Employee.Builder> employeeList;
    String employeeSeqUri;

    public EmployeeProtoHDFSWriter(ArrayList<EmployeeOuterClass.Employee.Builder> employeeList,
                                   String employeeSeqUri) {
        this.employeeList = employeeList;
        this.employeeSeqUri = employeeSeqUri;
    }

    @Override
    public void write() {
        Configuration config = ConfigurationClass.getConfiguration();
        FileSystem fs = null;
        SequenceFile.Writer writer = null;
        SequenceFileWriter sequenceFileWriter = new SequenceFileWriter();
        try {
            fs = FileSystem.get(config);
            Path path = new Path(employeeSeqUri);
            int id = 0;
            //create sequence file if sequence file path does not exists
            if (!fs.exists(path)) {
                writer = sequenceFileWriter.getWriter(config, path);
                System.out.println("file created");
                for (EmployeeOuterClass.Employee.Builder employee : employeeList) {
                    //write proto object to file
                    writer.append(new IntWritable((++id)),
                            new ImmutableBytesWritable(employee.build().toByteArray()));
                }
            } else {
                System.out.println("Sequence file already exit!"); //file already exist
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(writer);
        }
    }

}
