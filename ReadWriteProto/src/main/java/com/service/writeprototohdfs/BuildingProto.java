package com.service.writeprototohdfs;

import com.util.IProto;
import com.util.protoobjects.BuildingOuterClass;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.io.IOUtils;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class BuildingProto implements IProto {
    private String buildingCsvFilePath;

    public BuildingProto(String buildingCsvFilePath) {
        this.buildingCsvFilePath = buildingCsvFilePath;
    }

    @Override
    public ArrayList<BuildingOuterClass.Building.Builder> populateNRecords() {
        ArrayList<BuildingOuterClass.Building.Builder> buildingList = new ArrayList<BuildingOuterClass.Building.Builder>();
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(buildingCsvFilePath));
            boolean columnRead = false;
            //reading records from csv file
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
            for (CSVRecord record : records) {
                if (columnRead) {
                    setProtoFields(buildingList, record);
                }
                columnRead = true;
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(reader);
        }
        return buildingList;
    }

    private void setProtoFields(ArrayList<BuildingOuterClass.Building.Builder> buildingList, CSVRecord record) {
        BuildingOuterClass.Building.Builder building = BuildingOuterClass.Building.newBuilder();
        building.setBuildingCode(Integer.parseInt(record.get(0)));
        building.setTotalFloors(Integer.parseInt(record.get(1)));
        building.setCompaniesInTheBuilding(Integer.parseInt(record.get(2)));
        building.setCafeteriaCode(record.get(3));
        //adding building proto to list
        buildingList.add(building);
    }


}

