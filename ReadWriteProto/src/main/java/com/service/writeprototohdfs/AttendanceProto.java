package com.service.writeprototohdfs;

import com.google.protobuf.Timestamp;
import com.util.IProto;
import com.util.protoobjects.AttendanceOuterClass;

import java.time.Instant;
import java.time.Period;
import java.util.ArrayList;
import java.util.Random;

public class AttendanceProto implements IProto {
    private static final int NOOFEMPLOYEE = 100;
    private static final String ATTENDANCE_START_TIMESTAMP = "2020-04-01T10:00:00.00Z";
    private static final int ATTENDANCE_NO_OF_DAYS = 15;

    @Override
    public ArrayList<AttendanceOuterClass.Attendance.Builder> populateNRecords() {
        ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList =
                new ArrayList<AttendanceOuterClass.Attendance.Builder>();

        for (int employee_id = 1; employee_id <= NOOFEMPLOYEE; employee_id++) {
            AttendanceOuterClass.Attendance.Builder attendance =
                    AttendanceOuterClass.Attendance.newBuilder();
            attendance.setEmployeeId(employee_id);
            //parsing timestamp filed
            Instant instant = Instant.parse(ATTENDANCE_START_TIMESTAMP);
            AttendanceOuterClass.DatePresence.Builder datePresence =
                    AttendanceOuterClass.DatePresence.newBuilder();
            setDatePresence(attendance, instant, datePresence);
            //adding attendance proto to list
            attendanceList.add(attendance);
        }
        return attendanceList;
    }

    private void setDatePresence(AttendanceOuterClass.Attendance.Builder attendance,
                                 Instant instant, AttendanceOuterClass.DatePresence.Builder datePresence) {
        Random random_no = new Random();
        for (int i = 1; i <= ATTENDANCE_NO_OF_DAYS; i++) {
            Timestamp timestamp = Timestamp.newBuilder().setSeconds(instant.getEpochSecond())
                    .setNanos(instant.getNano()).build();
            datePresence.setDate(timestamp);
            //Generate random boolean values for employee isPresent field
            boolean is_present = random_no.nextBoolean();
            datePresence.setIsPresent(is_present);
            // increasing days to days+1
            instant = instant.plus(Period.ofDays((1)));
            attendance.addDatesPresent(datePresence);
        }
    }
}
