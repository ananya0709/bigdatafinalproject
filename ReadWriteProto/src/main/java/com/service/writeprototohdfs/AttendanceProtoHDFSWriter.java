package com.service.writeprototohdfs;

import com.config.HadoopConfiguration;
import com.config.VariableSetter;
import com.util.ConfigurationClass;
import com.util.IProtoHDFSWriter;
import com.util.SequenceFileWriter;
import com.util.protoobjects.AttendanceOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.util.ArrayList;

public class AttendanceProtoHDFSWriter implements IProtoHDFSWriter {
    ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList;
    String attendanceSeqUri;

    public AttendanceProtoHDFSWriter(ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList,
                                     String attendanceSeqUri) {
        this.attendanceList = attendanceList;
        this.attendanceSeqUri = attendanceSeqUri;
    }

    @Override
    public void write() {
        Configuration config = ConfigurationClass.getConfiguration();
        FileSystem fs = null;
        SequenceFile.Writer writer = null;
        SequenceFileWriter sequenceFileWriter = new SequenceFileWriter();
        try {
            fs = FileSystem.get(config);
            Path path = new Path(attendanceSeqUri);
            int id = 0;
            //create sequence file if file path does not exists
            if (!fs.exists(path)) {
                writer = sequenceFileWriter.getWriter(config, path);
                System.out.println("file created");
                for (AttendanceOuterClass.Attendance.Builder attendance : attendanceList) {
                    //write proto object to file
                    writer.append(new IntWritable((++id)),
                            new ImmutableBytesWritable(attendance.build().toByteArray()));
                }
            } else {
                System.out.println("Sequence file already exist!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(writer);
        }
    }
}
