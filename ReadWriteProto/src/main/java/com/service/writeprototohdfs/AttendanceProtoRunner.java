package com.service.writeprototohdfs;

import com.util.protoobjects.AttendanceOuterClass;

import java.util.ArrayList;

public class AttendanceProtoRunner {

    private String attendanceSeqUri;

    public AttendanceProtoRunner(String attendanceSeqUri) {
        this.attendanceSeqUri = attendanceSeqUri;
    }

    public void runProto() {
        AttendanceProto attendanceProto = new AttendanceProto();
        ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList =
                attendanceProto.populateNRecords();

        //Writing Attendance Proto to HDFS
        AttendanceProtoHDFSWriter attendanceProtoHDFSWriter = new AttendanceProtoHDFSWriter(attendanceList,
                attendanceSeqUri);
        attendanceProtoHDFSWriter.write();
    }
}
