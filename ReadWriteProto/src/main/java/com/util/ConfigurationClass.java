package com.util;

import com.config.HadoopConfiguration;
import com.config.VariableSetter;
import org.apache.hadoop.conf.Configuration;

public class ConfigurationClass {
    public static Configuration getConfiguration() {
        HadoopConfiguration conf = new HadoopConfiguration(VariableSetter.RESOURCE1, VariableSetter.RESOURCE2);
        Configuration config = conf.getConfiguration();
        return config;
    }
}
