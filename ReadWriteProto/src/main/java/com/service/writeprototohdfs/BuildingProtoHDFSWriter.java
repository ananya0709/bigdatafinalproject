package com.service.writeprototohdfs;

import com.config.HadoopConfiguration;
import com.config.VariableSetter;
import com.util.ConfigurationClass;
import com.util.IProtoHDFSWriter;
import com.util.SequenceFileWriter;
import com.util.protoobjects.BuildingOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.util.ArrayList;

public class BuildingProtoHDFSWriter implements IProtoHDFSWriter {
    ArrayList<BuildingOuterClass.Building.Builder> buildingList;
    String buildingSeqUri;

    public BuildingProtoHDFSWriter(ArrayList<BuildingOuterClass.Building.Builder> buildingList, String buildingSeqUri) {
        this.buildingList = buildingList;
        this.buildingSeqUri = buildingSeqUri;
    }

    @Override
    public void write(){
        Configuration config = ConfigurationClass.getConfiguration();
        FileSystem fs = null;
        SequenceFile.Writer writer = null;
        SequenceFileWriter sequenceFileWriter = new SequenceFileWriter();
        try {
            fs = FileSystem.get(config);
            Path path = new Path(buildingSeqUri);
            int id = 0;
            //create sequence file if sequence file path does not exists
            if (!fs.exists(path)) {
                writer = sequenceFileWriter.getWriter(config, path);
                System.out.println("file created");
                    for (BuildingOuterClass.Building.Builder building : buildingList) {
                        //write proto object to file
                        writer.append(new IntWritable((++id)),
                                new ImmutableBytesWritable(building.build().toByteArray()));
                }
            } else {
                System.out.println("Sequence file already exist!"); //file already exist
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(writer);
        }

    }

}
