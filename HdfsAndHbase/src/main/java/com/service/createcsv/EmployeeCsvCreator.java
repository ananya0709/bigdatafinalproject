package com.service.createcsv;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.generator.defaults.IntGenerator;
import com.github.javafaker.Faker;
import com.util.ICsvCreator;

import java.util.Random;

public class EmployeeCsvCreator implements ICsvCreator {
    enum FloorNumber {
        GROUND,
        FIRST,
        SECOND
    }

    @Override
    public void create(int noOfFilesToGenerate, String path, int noOfRowsToGenerate) {
        for (int i = 1; i <= noOfFilesToGenerate; i++) {
            int pick = new Random().nextInt(FloorNumber.values().length);
            Faker faker = Faker.instance();
            Gen.start();
            Gen.start()
                    .addField("name", () -> faker.name().firstName())
                    .addField("employee_id", new IntGenerator(1, 100))
                    .addField("building_code", new IntGenerator(101, 110))
                    .addField("floor_number", () -> FloorNumber.values()[pick])
                    .addField("salary", new IntGenerator(300000, 800000))
                    .addField("department", () -> faker.company().industry().replaceAll(",", ""))
                    .generate(noOfRowsToGenerate)
                    .asCsv()
                    .toFile(path + "employee" + i + ".csv");
        }
    }

}
