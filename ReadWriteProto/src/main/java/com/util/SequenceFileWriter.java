package com.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;

public class SequenceFileWriter {
    
    public SequenceFile.Writer getWriter(Configuration config, Path path) throws IOException {
        IntWritable key = new IntWritable(); //key as INtWritable
        ImmutableBytesWritable value = new ImmutableBytesWritable(); //value as ImmutableByteWritable
        return SequenceFile.createWriter(config, SequenceFile.Writer.file(path),
                SequenceFile.Writer.keyClass(key.getClass()),
                ArrayFile.Writer.valueClass(value.getClass()));
    }
}
