package com.builder;

import com.config.VariableSetter;
import com.service.lowestattendance.AttendanceDriver;
import com.service.loadprototohbase.BuildingHbaseDriver;
import com.service.loadprototohbase.EmployeeHbaseDriver;
import com.service.populatecafeteriacode.EmployeePostProcessorDriver;
import com.service.writeprototohdfs.*;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.util.ToolRunner;

public class ProtoRunner {

    private static final String EMPLOYEE_CSV_FILE_PATH =
            VariableSetter.CREATE_EMP_BUILDING_CSV_PATH + "employee1.csv";
    private static final String BUILDING_CSV_FILE_PATH =
            VariableSetter.CREATE_EMP_BUILDING_CSV_PATH + "building1.csv";

    private static final String EMPLOYEE_URI = "hdfs://localhost:8020/ProtoFiles/employee.seq";
    private static final String BUILDING_URI = "hdfs://localhost:8020/ProtoFiles/building.seq";
    private static final String ATTENDANCE_URI = "hdfs://localhost:8020/ProtoFiles/attendance.seq";

    public void buildTask(String[] args) {
        populateRecords();
        loadProtoToHbaseTask(args);
        joinTask(args);
    }

    private void populateRecords() {
        EmployeeProtoRunner employeeProtoRunner =
                new EmployeeProtoRunner(EMPLOYEE_CSV_FILE_PATH, EMPLOYEE_URI);

        BuildingProtoRunner buildingProtoRunner = new BuildingProtoRunner(BUILDING_URI,
                BUILDING_CSV_FILE_PATH);

        AttendanceProtoRunner attendanceProtoRunner =  new AttendanceProtoRunner(ATTENDANCE_URI);

        employeeProtoRunner.runProto(); //load csv into employee proto
        buildingProtoRunner.runProto(); //load csv into building proto
        attendanceProtoRunner.runProto(); //load csv into attendance proto

    }


    private void loadProtoToHbaseTask(String[] args)  {
        BuildingHbaseDriver buildingHbaseDriver = new BuildingHbaseDriver();
        EmployeeHbaseDriver employeeHbaseDriver = new EmployeeHbaseDriver();

        try {
            //run job for loading building proto to Hbase
            ToolRunner.run(HBaseConfiguration.create(), buildingHbaseDriver, args);
            //run job for loading employee proto to Hbase
            ToolRunner.run(HBaseConfiguration.create(), employeeHbaseDriver, args);
            //run() method of ToolRunner class itself throws Exception
        }
        //run method of ToolRunner class throws Exception
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void joinTask(String[] args) {
        EmployeePostProcessorDriver employeePostProcessorDriver = new EmployeePostProcessorDriver();
        AttendanceDriver attendanceDriver = new AttendanceDriver();

            /*run job for joining employee with attendance to
            find the employee with the lowest attendance*/
        employeePostProcessorDriver.run(args);
        attendanceDriver.run(args);
    }
}


