package com.service.bulkupload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;

public class BulkLoadDriver extends Configured implements Tool {
    private String bulkLoadInPathFiles;
    private String bulkLoadJobName;
    private String bulkLoadOuthPath;
    private String tableName;

    public BulkLoadDriver(String bulkLoadInPathFiles, String bulkLoadJobName,
                          String bulkLoadOuthPath, String tableName) {
        this.bulkLoadInPathFiles = bulkLoadInPathFiles;
        this.bulkLoadJobName = bulkLoadJobName;
        this.bulkLoadOuthPath = bulkLoadOuthPath;
        this.tableName = tableName;
    }


    private Job createSubmittableJob(Configuration conf, String tableNameString, Path tmpPath, String input) {
        System.out.println("conf: " + conf);
        Job job = null;
        try {
            job = Job.getInstance(conf, bulkLoadJobName);
            job.setJarByClass(BulkLoadMapper.class);
            job.setMapperClass(BulkLoadMapper.class); //set mapper class
            job.setMapOutputKeyClass(ImmutableBytesWritable.class); //set map output key class
            job.setMapOutputValueClass(Put.class); //set map output value class
            FileOutputFormat.setOutputPath(job, tmpPath); //set output path
            FileInputFormat.addInputPath(job, new Path(input)); //add input path
            configureLoad(conf, tableNameString, job);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return job;
    }

    private void configureLoad(Configuration conf, String tableNameString, Job job) throws IOException {
        Connection connection = ConnectionFactory.createConnection(conf);
        TableName tableName = TableName.valueOf(tableNameString);
        Table table = connection.getTable(tableName);
        RegionLocator regionLocator = connection.getRegionLocator(tableName);
        HFileOutputFormat2.configureIncrementalLoad(job, table, regionLocator); //Incremental load into hbase
    }

    @Override
    public int run(String[] args) {
        Configuration conf = HBaseConfiguration.create(getConf());
        setConf(conf);
        String tableNameString = tableName;
        Path tmpPath = new Path(bulkLoadOuthPath);
        String input = bulkLoadInPathFiles;
        Job job = createSubmittableJob(conf, tableNameString, tmpPath, input);
        boolean success = false;
        try {
            success = job.waitForCompletion(true);
            doBulkLoad(tableNameString, tmpPath);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return success ? 0 : 1;
    }

    private void doBulkLoad(String tableNameString, Path tmpPath) throws IOException {
        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(getConf());
        Connection connection = ConnectionFactory.createConnection(getConf());
        Admin admin = connection.getAdmin();
        TableName tableName = TableName.valueOf(tableNameString);
        Table table = connection.getTable(tableName);
        RegionLocator regionLocator = connection.getRegionLocator(tableName);
        loader.doBulkLoad(tmpPath, admin, table, regionLocator);

    }
}
