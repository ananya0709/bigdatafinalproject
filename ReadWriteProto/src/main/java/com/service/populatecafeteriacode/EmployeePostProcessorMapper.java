package com.service.populatecafeteriacode;

import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class EmployeePostProcessorMapper extends TableMapper<LongWritable, ImmutableBytesWritable> {
    private static final byte[] BUILDINGTABLE = Bytes.toBytes("building");
    private static final byte[] EMPLOYEETABLE = Bytes.toBytes("employee");
    public static final byte[] EMPCF = Bytes.toBytes("employee_details");
    public static final byte[] EMPATTR1 = Bytes.toBytes("employee_qual");
    private static final String BUILDINGURI = "hdfs://localhost:8020/ProtoFiles/building.seq";
    private HashMap<String, String> buildingHashMap;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        //initialising buildingHashMap with building code and cafeteria code
        CodeLookupBuilder codeLookupBuilder = new CodeLookupBuilder();
        buildingHashMap = codeLookupBuilder.get(BUILDINGURI);
        System.out.println("buildingHashMap : " + buildingHashMap);
    }

    @Override
    public void map(ImmutableBytesWritable rowKey, Result columns, Context context) {
        TableSplit currentSplit = (TableSplit) context.getInputSplit();
        byte[] tableName = currentSplit.getTableName();
        try {
            if (Arrays.equals(tableName, BUILDINGTABLE)) {
                //do nothing
            } else if (Arrays.equals(tableName, EMPLOYEETABLE)) {
                ImmutableBytesWritable value = new ImmutableBytesWritable();
                value.set(columns.getValue(EMPCF, EMPATTR1));
                EmployeeOuterClass.Employee.Builder employee =
                        EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get());

                String building_code = String.valueOf(employee.getBuildingCode());
                String row = new String(rowKey.get());

                //if buildingHashMap contains building code then set employee object with cafeteria code
                if (buildingHashMap.containsKey(building_code)) {
                    employee.setCafeteriaCode(buildingHashMap.get(building_code));
                }
                //write the context with employee object
                context.write(new LongWritable(Long.parseLong(row)),
                        new ImmutableBytesWritable(employee.build().toByteArray()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
