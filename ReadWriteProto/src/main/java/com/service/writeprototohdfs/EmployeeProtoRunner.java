package com.service.writeprototohdfs;

import com.util.protoobjects.BuildingOuterClass;
import com.util.protoobjects.EmployeeOuterClass;

import java.util.ArrayList;

public class EmployeeProtoRunner {
    private String employeeSeqUri;
    private String employeeCsvFilePath;

    public EmployeeProtoRunner(String employeeCsvFilePath, String employeeSeqUri) {
        this.employeeCsvFilePath = employeeCsvFilePath;
        this.employeeSeqUri = employeeSeqUri;
    }

    public void runProto() {
        EmployeeProto employeeProto = new EmployeeProto(employeeCsvFilePath);
        ArrayList<EmployeeOuterClass.Employee.Builder> employeeList =
                employeeProto.populateNRecords();

        //Writing Employee Proto to HDFS
        EmployeeProtoHDFSWriter employeeProtoHDFSWriter = new EmployeeProtoHDFSWriter(employeeList,
                employeeSeqUri);
        employeeProtoHDFSWriter.write();
    }

}
