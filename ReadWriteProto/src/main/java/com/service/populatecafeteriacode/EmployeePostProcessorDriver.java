package com.service.populatecafeteriacode;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeePostProcessorDriver extends Configured implements Tool {
    private static final String JOB_NAME = "Join Hbase Tables";
    private static final String BUILDING_TABLE_NAME = "building";
    private static final String EMPLOYEE_TABLE_NAME ="employee";
    private static final String EMPLOYEE_OUPUT_PATH = "hdfs://localhost:8020" +
            "/PopulateCafeteriaCodeForEmployee";


    @Override
    public int run(String[] arg0) {
        List<Scan> scans = new ArrayList<Scan>();
        //adding table name to scan list
        scans.add(scan(BUILDING_TABLE_NAME));
        scans.add(scan(EMPLOYEE_TABLE_NAME));
        HBaseConfiguration conf = new HBaseConfiguration();
        Job job = null;
        try {
            job = new Job(conf, JOB_NAME);
            job.setJarByClass(EmployeePostProcessorDriver.class);
            job.setNumReduceTasks(0); //set number of reducer to 0
            FileOutputFormat.setOutputPath(job, new Path(EMPLOYEE_OUPUT_PATH));
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
            job.setOutputKeyClass(LongWritable.class); //set output key class
            job.setOutputValueClass(ImmutableBytesWritable.class); //set output value class
            TableMapReduceUtil.initTableMapperJob(scans, //passing hbase table to mapper function
                    EmployeePostProcessorMapper.class, LongWritable.class,
                    ImmutableBytesWritable.class, job);

            job.waitForCompletion(true);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Scan scan(String tableName)
    {
        Scan scan = new Scan();
        scan.setAttribute("scan.attributes.table.name", Bytes.toBytes(tableName));
        return scan;
    }

}