package com.service.writeprototohdfs;

import com.util.IProto;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.io.IOUtils;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class EmployeeProto implements IProto {

    private String employeeCsvFilePath;

    public EmployeeProto(String employeeCsvFilePath) {
        this.employeeCsvFilePath = employeeCsvFilePath;
    }

    @Override
    public ArrayList<EmployeeOuterClass.Employee.Builder> populateNRecords() {
        ArrayList<EmployeeOuterClass.Employee.Builder> employeeList = new ArrayList<>();
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(employeeCsvFilePath));
            boolean columnRead = false;
            //reading records from csv file
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
            for (CSVRecord record : records) {
                if (columnRead) {
                    setProtoFields(employeeList, record);
                }
                columnRead = true;
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(reader);
        }
        return employeeList;
    }

    private void setProtoFields(ArrayList<EmployeeOuterClass.Employee.Builder> employeeList,
                                CSVRecord record) {
        EmployeeOuterClass.Employee.Builder employee = EmployeeOuterClass.Employee.newBuilder();
        employee.setName(record.get(0));
        employee.setEmployeeId(Integer.parseInt(record.get(1)));
        employee.setBuildingCode(Integer.parseInt(record.get(2)));
        String floor_name = record.get(3);
        EmployeeOuterClass.Employee.FloorNumber floor = null;

        if (floor_name.equals("GROUND")) {
            floor = EmployeeOuterClass.Employee.FloorNumber.GROUND;
        } else if (floor_name.equals("FIRST")) {
            floor = EmployeeOuterClass.Employee.FloorNumber.FIRST;
        } else if (floor_name.equals("SECOND")) {
            floor = EmployeeOuterClass.Employee.FloorNumber.SECOND;
        }
        employee.setFloorNumber(floor);
        employee.setSalary(Integer.parseInt(record.get(4)));
        employee.setDepartment(record.get(5));
        //adding employee proto to list
        employeeList.add(employee);
    }

}
