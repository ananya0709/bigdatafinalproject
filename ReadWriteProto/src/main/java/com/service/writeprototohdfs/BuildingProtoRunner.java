package com.service.writeprototohdfs;

import com.util.protoobjects.AttendanceOuterClass;
import com.util.protoobjects.BuildingOuterClass;

import java.util.ArrayList;

public class BuildingProtoRunner {

    private String buildingSeqUri;
    private String buildingCsvFilePath;

    public BuildingProtoRunner(String buildingSeqUri, String buildingCsvFilePath) {
        this.buildingSeqUri = buildingSeqUri;
        this.buildingCsvFilePath = buildingCsvFilePath;
    }

    public void runProto() {
        BuildingProto buildingProto = new BuildingProto(buildingCsvFilePath);
        ArrayList<BuildingOuterClass.Building.Builder> buildingList =
                buildingProto.populateNRecords();

        //Writing Building Proto to HDFS
        BuildingProtoHDFSWriter buildingProtoHDFSWriter = new BuildingProtoHDFSWriter(buildingList,
                buildingSeqUri);
        buildingProtoHDFSWriter.write();
    }
}

