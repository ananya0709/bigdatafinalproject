package com.builder;

import com.config.VariableSetter;
import com.service.bulkupload.BulkLoadDriver;
import com.service.csvtohdfs.TaskRunner;
import com.service.wordcount.WordCountDriver;
import org.apache.hadoop.util.ToolRunner;

public class Builder {
    private static final String TABLE_NAME = "peopletable";
    private static final String WORD_COUNT_INPUT_PATH ="hdfs://localhost:8020/WordCount/InputFolder";
    private static final String WORD_COUNT_OUTPUT_PATH ="hdfs://localhost:8020/WordCountOutput";
    private static final String WORD_COUNT_JOB_NAME = "wordcount";

    private static final String BULK_LOAD_IN_PATH_FILES = "hdfs://localhost:8020/CSVFolder";
    private static final String BULK_LOAD_JOB_NAME = "HBase Bulk Load Example";
    private static final String BULK_LOAD_OUT_PATH = "hdfs://localhost:8020/BulkUploadHbase";

    public void buildTask(String[] args) throws Exception{
        //Executing Load CSV to Hdfs Job
        TaskRunner taskObj = new TaskRunner(VariableSetter.CREATE_PEOPLE_CSV_PATH,
                VariableSetter.CREATE_EMP_BUILDING_CSV_PATH, VariableSetter.RESOURCE1,
                VariableSetter.RESOURCE2, TABLE_NAME);
        taskObj.runTask();

        //Executing WordCount Job
        WordCountDriver wordCountDriver = new WordCountDriver(WORD_COUNT_INPUT_PATH,
                WORD_COUNT_JOB_NAME, WORD_COUNT_OUTPUT_PATH);
        wordCountDriver.runJob();

        //Executing Bulk load Hbase Job
        BulkLoadDriver bulkLoadDriver = new BulkLoadDriver(BULK_LOAD_IN_PATH_FILES,
                BULK_LOAD_JOB_NAME, BULK_LOAD_OUT_PATH, TABLE_NAME);
       ToolRunner.run(bulkLoadDriver, args);
    }
}
