package com.util;

@FunctionalInterface
public interface IProto {
    <T> T populateNRecords();
}
