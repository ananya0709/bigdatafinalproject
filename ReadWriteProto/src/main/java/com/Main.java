package com;

import com.builder.ProtoRunner;

public class Main {

    public static void main(String[] args) throws Exception {
        ProtoRunner protoRunner = new ProtoRunner();
        protoRunner.buildTask(args);
    }
}
